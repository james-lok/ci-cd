# Bashscript to run website on AWS
read -r varname < ./input_ips.txt
key=$1
scp -i  $key -r james_website ec2-user@$varname:
scp -i  $key bashscript_install_aws.sh ec2-user@$varname:
ssh -i $key ec2-user@$varname bash bashscript_install_aws.sh